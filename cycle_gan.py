import keras
import tensorflow as tf

LEARNING_RATE = 2e-4


def mse_loss(y_true, y_pred):
    return tf.reduce_mean(keras.losses.mean_squared_error(y_true, y_pred))


@keras.saving.register_keras_serializable()
class CycleGan(keras.Model):
    def __init__(
        self,
        f: keras.Model,
        g: keras.Model,
        dx: keras.Model,
        dy: keras.Model,
        lambda_: float,
    ):
        super(CycleGan, self).__init__(name="cyclegan")
        self.f = f  # Monet -> photo generator
        self.g = g  # photo -> Monet generator
        self.dx = dx  # photo discriminator
        self.dy = dy  # Monet discriminator
        self.lambda_ = lambda_  # cycle and identity loss weight

    def compile(self, learning_rate=LEARNING_RATE):
        super().compile()
        self.f_optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
        self.g_optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
        self.dx_optimizer = keras.optimizers.Adam(learning_rate=learning_rate)
        self.dy_optimizer = keras.optimizers.Adam(learning_rate=learning_rate)

    def call(self, data, training=None):
        real_x, real_y = data

        fake_y = self.f(real_x, training=training)
        fake_x = self.g(real_y, training=training)
        cycled_x = self.g(fake_y, training=training)
        cycled_y = self.f(fake_x, training=training)
        same_x = self.g(real_x, training=training)
        same_y = self.f(real_y, training=training)

        disc_real_x = self.dx(real_x, training=training)
        disc_real_y = self.dy(real_y, training=training)
        disc_fake_x = self.dx(fake_x, training=training)
        disc_fake_y = self.dy(fake_y, training=training)

        return (
            fake_x,
            fake_y,
            cycled_x,
            cycled_y,
            same_x,
            same_y,
            disc_real_x,
            disc_real_y,
            disc_fake_x,
            disc_fake_y,
        )

    def train_step(self, data):
        real_x, real_y = data
        with tf.GradientTape(persistent=True) as tape:
            (
                fake_x,
                fake_y,
                cycled_x,
                cycled_y,
                same_x,
                same_y,
                disc_real_x,
                disc_real_y,
                disc_fake_x,
                disc_fake_y,
            ) = self(data, training=True)

            cycle_loss = mse_loss(real_x, cycled_x) + mse_loss(real_y, cycled_y)
            gen_f_loss = (
                keras.losses.binary_crossentropy(
                    tf.ones_like(disc_fake_y), disc_fake_y
                )  # discriminator loss
                + self.lambda_ * cycle_loss  # cycle loss
                + self.lambda_ * 0.5 * mse_loss(real_y, same_y)  # identity loss
            )
            gen_g_loss = (
                keras.losses.binary_crossentropy(
                    tf.ones_like(disc_fake_x), disc_fake_x
                )  # discriminator loss
                + self.lambda_ * cycle_loss  # cycle loss
                + self.lambda_ * 0.5 * mse_loss(real_x, same_x)  # identity loss
            )
            disc_x_loss = 0.5 * (
                keras.losses.binary_crossentropy(
                    tf.ones_like(disc_real_x), disc_real_x
                )  # original detection
                + keras.losses.binary_crossentropy(
                    tf.zeros_like(disc_fake_x), disc_fake_x
                )  # fake detection
            )
            disc_y_loss = 0.5 * (
                keras.losses.binary_crossentropy(
                    tf.ones_like(disc_real_y), disc_real_y
                )  # original detection
                + keras.losses.binary_crossentropy(
                    tf.zeros_like(disc_fake_y), disc_fake_y
                )  # fake detection
            )

        grad_f = tape.gradient(gen_f_loss, self.f.trainable_variables)
        grad_g = tape.gradient(gen_g_loss, self.g.trainable_variables)
        grad_dx = tape.gradient(disc_x_loss, self.dx.trainable_variables)
        grad_dy = tape.gradient(disc_y_loss, self.dy.trainable_variables)

        self.f_optimizer.apply_gradients(zip(grad_f, self.f.trainable_variables))
        self.g_optimizer.apply_gradients(zip(grad_g, self.g.trainable_variables))
        self.dx_optimizer.apply_gradients(zip(grad_dx, self.dx.trainable_variables))
        self.dy_optimizer.apply_gradients(zip(grad_dy, self.dy.trainable_variables))

        return {
            "gen_f_loss": gen_f_loss,
            "gen_g_loss": gen_g_loss,
            "disc_x_loss": disc_x_loss,
            "disc_y_loss": disc_y_loss,
        }

    def get_config(self):
        config = super().get_config()
        config["f"] = self.f.get_config()
        config["g"] = self.g.get_config()
        config["dx"] = self.dx.get_config()
        config["dy"] = self.dy.get_config()
        config["lambda_"] = self.lambda_
        return config

    @classmethod
    def from_config(cls, config):
        f = keras.Model.from_config(config["f"])
        g = keras.Model.from_config(config["g"])
        dx = keras.Model.from_config(config["dx"])
        dy = keras.Model.from_config(config["dy"])
        lambda_ = config["lambda_"]
        return cls(f, g, dx, dy, lambda_)
