#!/bin/bash

docker build -t monet-generator:latest .
docker run --gpus all --rm --name monet-generator -v $(pwd)/data:/app/data:ro -v $(pwd)/output:/app/output monet-generator:latest
