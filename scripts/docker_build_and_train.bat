docker build -t monet-generator:latest .
docker run --gpus all --rm --name monet-generator -v %cd%/data:/app/data:ro -v %cd%/output:/app/output monet-generator:latest
