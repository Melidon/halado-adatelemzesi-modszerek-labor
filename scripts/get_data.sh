#!/bin/bash

## Donwload your API key from Kaggle and place it in the .kaggle folder

## Install kaggle API first: https://github.com/Kaggle/kaggle-api
# pip install kaggle

kaggle competitions download -c gan-getting-started
unzip gan-getting-started.zip -d data
rm gan-getting-started.zip
