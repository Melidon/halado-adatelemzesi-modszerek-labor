import numpy as np
import tensorflow as tf
import keras
from cycle_gan import CycleGan
import gradio as gr


IMAGE_SIZE = [256, 256]


def numpy_to_tensor(image: np.ndarray) -> tf.Tensor:
    image = tf.image.resize(image, IMAGE_SIZE)
    image = image * 2.0 / 255.0 - 1.0
    image = tf.clip_by_value(image, -1.0, 1.0)
    image = tf.expand_dims(image, axis=0)
    return image


def tensor_to_numpy(image: tf.Tensor) -> np.ndarray:
    image = tf.squeeze(image, axis=0)
    image = (image + 1.0) / 2.0 * 255.0
    image = tf.clip_by_value(image, 0.0, 255.0)
    image = image.numpy().astype(np.uint8)
    return image


model = keras.models.load_model(
    "output/best_f_loss.keras",
    custom_objects={"CycleGan": CycleGan},
)


def to_monet(numpy_input: np.ndarray):
    tensor_input = numpy_to_tensor(numpy_input)
    tensor_output = model.f(tensor_input, training=False)
    numpy_output = tensor_to_numpy(tensor_output)
    return numpy_output


gradio_app = gr.Interface(
    to_monet,
    inputs=gr.Image(),
    outputs=gr.Image(),
    title="I'm Something of a Painter Myself",
)


if __name__ == "__main__":
    gradio_app.launch()
