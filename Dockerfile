FROM python:3.12 AS nbconverter
WORKDIR /app
RUN pip install --no-cache-dir --upgrade jupyter==1.0.0
COPY main.ipynb main.ipynb
RUN jupyter nbconvert --to script main.ipynb


FROM tensorflow/tensorflow:2.16.1-gpu AS trainer
WORKDIR /app
# TODO: Use requirements.txt
RUN pip install --no-cache-dir matplotlib
COPY --from=nbconverter /app/main.py main.py
CMD ["python3", "main.py"]
