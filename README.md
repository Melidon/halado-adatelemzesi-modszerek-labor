# I’m Something of a Painter Myself

## Members

| Name            | NEPTUN |
| --------------- | ------ |
| Somorjai Márk   | FAFSAG |
| Szakszon Mátyás | DXR372 |
| Szommer Zsombor | MM5NOT |

## Project Plan

- We are taking part in the Kaggle competition [I’m Something of a Painter Myself](https://www.kaggle.com/competitions/gan-getting-started/overview) to generate Monet-style images.
- We will use GANs to generate Monet-style images.
- We are evaluating our model using the MiFID score (see below for more details).
- Our goal is to reach the top 25% of the leaderboard, which is currently at a MiFID score of 52.

## How to train the model in Docker

Run the training script:

```bash
sudo ./scripts/docker_build_and_train.sh
```

## Description

We recognize the works of artists through their unique style, such as color choices or brush strokes.
The "je ne sais quoi" of artists like Claude Monet can now be imitated with algorithms thanks to generative adversarial networks (GANs).
In this getting started competition, you will bring that style to your photos or recreate the style from scratch!

Computer vision has advanced tremendously in recent years and GANs are now capable of mimicking objects in a very convincing way.
But creating museum-worthy masterpieces is thought of to be, well, more art than science.
So can (data) science, in the form of GANs, trick classifiers into believing you’ve created a true Monet?
That’s the challenge you’ll take on!

### The Challenge:

A GAN consists of at least two neural networks: a generator model and a discriminator model.
The generator is a neural network that creates the images.
For our competition, you should generate images in the style of Monet.
This generator is trained using a discriminator.

The two models will work against each other, with the generator trying to trick the discriminator, and the discriminator trying to accurately classify the real vs. generated images.

Your task is to build a GAN that generates 7,000 to 10,000 Monet-style images.

## Evaluation

### MiFID

Submissions are evaluated on MiFID (Memorization-informed Fréchet Inception Distance), which is a modification from [Fréchet Inception Distance (FID)](https://arxiv.org/abs/1706.08500).

The smaller MiFID is, the better your generated images are.

### What is FID?

Originally published [here](https://arxiv.org/abs/1706.08500) ([github](https://github.com/bioinf-jku/TTUR)), FID, along with [Inception Score (IS)](https://arxiv.org/abs/1606.03498), are both commonly used in recent publications as the standard for evaluation methods of GANs.

In FID, we use the Inception network to extract features from an intermediate layer.
Then we model the data distribution for these features using a multivariate Gaussian distribution with mean $\mu$ and covariance $\Sigma$.
The FID between the real images $r$ and generated images $g$ is computed as:

$$
\text{FID} = ||\mu_r - \mu_g||^2 + \text{Tr} (\Sigma_r + \Sigma_g - 2 (\Sigma_r \Sigma_g)^{1/2})
$$

where $Tr$ sums up all the diagonal elements. FID is calculated by computing the Fréchet distance between two Gaussians fitted to feature representations of the Inception network.

### What is MiFID (Memorization-informed FID)?

In addition to FID, Kaggle takes training sample memorization into account.

The memorization distance is defined as the minimum cosine distance of all training samples in the feature space, averaged across all user generated image samples. This distance is thresholded, and it's assigned to 1.0 if the distance exceeds a pre-defined epsilon.

In mathematical form:

$$
d_{ij} = 1 - cos(f_{gi}, f_{rj}) = 1 - \frac{f_{gi} \cdot f_{rj}}{|f_{gi}| |f_{rj}|}
$$

where $f_g$ and $f_r$ represent the generated/real images in feature space (defined in pre-trained networks);
and $f_{gi}$ and $f_{rj}$ represent the $i^{th}$ and $j^{th}$ vectors of $f_g$ and $f_r$, respectively.

$$
d = \frac{1}{N} \sum_{i} \min_j d_{ij}
$$

defines the minimum distance of a certain generated image ($i$) across all real images ($j$), then averaged across all the generated images.

![](https://storage.googleapis.com/kaggle-media/competitions/GAN/latex-img-150.png)

defines the threshold of the weight only applies when the ($d$) is below a certain empirically determined threshold.

Finally, this memorization term is applied to the FID:

$$
MiFID = FID \cdot \frac{1}{d_{thr}}
$$

### Kaggle's workflow calculating MiFID for public and private scores

Kaggle calculates public MiFID scores with the pre-train neural network [Inception](http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz), and the public images used for evaluation are the rest of the TFDS Monet paintings.
**Note that as a Getting Started competition there is no private leaderboard.**

A demo of our MiFID evaluation code can be seen [here](https://www.kaggle.com/wendykan/demo-mifid-metric-for-dog-image-generation-comp).

### Submission File

You are going to generate 7,000-10,000 Monet-style images that are in `jpg` format.
Their sizes should be 256x256x3 (RGB).
Then you need to zip those images and your output from your Kernel should only have **ONE** output file named `images.zip`.

Please note that Kaggle Kernels has a number of output files capped at 500.
We highly encourage you to either directly write to a `zip` file as you generate images, or create a folder at `../tmp` as your temporary directory.
