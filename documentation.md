# I’m Something of a Painter Myself

## Table of Contents

- [I’m Something of a Painter Myself](#im-something-of-a-painter-myself)
  - [Table of Contents](#table-of-contents)
  - [Members](#members)
  - [Introduction](#introduction)
  - [Project Plan](#project-plan)
  - [Description](#description)
    - [The Challenge:](#the-challenge)
  - [Architecture](#architecture)
    - [Model](#model)
    - [Generator](#generator)
    - [Discriminator](#discriminator)
    - [Training](#training)
      - [Forward](#forward)
      - [Backward](#backward)
      - [Identity](#identity)
  - [Evaluation](#evaluation)
    - [MiFID](#mifid)
    - [What is FID?](#what-is-fid)
    - [What is MiFID (Memorization-informed FID)?](#what-is-mifid-memorization-informed-fid)
    - [Kaggle's workflow calculating MiFID for public and private scores](#kaggles-workflow-calculating-mifid-for-public-and-private-scores)
    - [Submission File](#submission-file)
  - [Results](#results)
    - [Visualization](#visualization)
    - [Thoughts](#thoughts)
  - [Gradio](#gradio)
  - [Ethics and Fair Use in AI Art](#ethics-and-fair-use-in-ai-art)
    - [Ethical Considerations](#ethical-considerations)
  - [Different View Points on AI usage](#different-view-points-on-ai-usage)
    - [Is AI copyright is overreacted?](#is-ai-copyright-is-overreacted)
    - [AI is cheating!](#ai-is-cheating)
    - [Conclusion](#conclusion)
    - [Fair Use and Copyright](#fair-use-and-copyright)
    - [Conclusion](#conclusion-1)
  - [Bibliography](#bibliography)

<div style="page-break-after: always;"></div>

## Members

| Name            | NEPTUN |
| --------------- | ------ |
| Somorjai Márk   | FAFSAG |
| Szakszon Mátyás | DXR372 |
| Szommer Zsombor | MM5NOT |

## Introduction

We took part in the Kaggle competition [I’m Something of a Painter Myself](https://www.kaggle.com/competitions/gan-getting-started/overview) to generate Monet-style images.

## Project Plan 
- We will use GANs to generate Monet-style images.
- We are evaluating our model using the MiFID score (see below for more details).
- Our goal was to reach the top 25% of the leaderboard, which is currently at a MiFID score of 52.

## Description

We recognize the works of artists through their unique style, such as color choices or brush strokes.
The "je ne sais quoi" of artists like Claude Monet can now be imitated with algorithms thanks to generative adversarial networks (GANs).
In this getting started competition, you will bring that style to your photos or recreate the style from scratch!

Computer vision has advanced tremendously in recent years and GANs are now capable of mimicking objects in a very convincing way.
But creating museum-worthy masterpieces is thought of to be, well, more art than science.
So can (data) science, in the form of GANs, trick classifiers into believing you’ve created a true Monet?
That’s the challenge you’ll take on!

### The Challenge:

A GAN consists of at least two neural networks: a generator model and a discriminator model.
The generator is a neural network that creates the images.
For our competition, you should generate images in the style of Monet.
This generator is trained using a discriminator.

The two models will work against each other, with the generator trying to trick the discriminator, and the discriminator trying to accurately classify the real vs. generated images.

Your task is to build a GAN that generates 7,000 to 10,000 Monet-style images.

<div style="page-break-after: always;"></div>

## Architecture

### Model

GANs consist of at least two neural networks: a generator model and a discriminator model.
The generator is a neural network that transforms an image from one domain to another.
The discriminator is a neural network that classifies images as real or fake.
The two models work against each other, with the generator trying to trick the discriminator, and the discriminator trying to accurately classify the real vs. generated images.

The problem with normal GANs is that they we need to have a dataset of paired images.
This was not possible in our case, as we only had Monet-style images and no corresponding real images.

Our implementation utilizes a CycleGAN model.
[CycleGAN](https://arxiv.org/abs/1703.10593) extends the capability of GANs to transform between domains without unpaired examples.
This is achieved by introducing another generator-discriminator pair to the network, which carry out the inverse transformation.
The addition forces the network to preserve some information from the original domain during transformation, so that it can inverse transform back to the original domain, and not just generate a random image in the other domain.
The architecture of a CycleGAN is presented below.

![CycleGAN architecture](./doc/architecture.png)

In our case, the parts of CycleGAN correspond to:
- $X$: domain of real photos
- $Y$: domain of Monet paintings
- $F$: generator, transforming photos to Monet paintings
- $G$: generator, transforming Monet paintings to photos
- $D_X$: discriminator, telling real and fake photos apart
- $D_Y$: discriminator, telling real and fake Monet paintings apart

### Generator

The generators of our model are [UNET](http://arxiv.org/abs/1505.04597)s.
The UNET architecture is a convolutional neural network that is mostly used for image segmentation.
First it downsamples the input to a lower resolution image, then it is upsampled back to its original size, creating skip connections between layers at the same depth.

![UNET architecture](./doc/unet.jpg)

In our case we also apply instance normalization after down- and upsampling.

### Discriminator

The discriminators of our model downsample the input to a lower resolution image, followed by instance normalization, after which a dense layer produces the single verdict of the discriminator about the image's authenticity.

### Training

The model is built from the generators and discriminators as described above. It takes the photo - Monet painting pair $(x_{real}, y_{real}$ and applies the following transformations during training:

#### Forward

The real photo $x_{real}$ is fed to $F$, which transforms it into a fake Monet painting $y_{fake}$. The painting discriminator $D_Y$ receives $y_{fake}$ and $y_{real}$, classifies them resulting in the loss $L_Y$ that is used to train both $F$ and $D_Y$, just like a regular GAN. Unlike a regular GAN, the fake Monet painting $y_{fake}$ is transformed back to a photo $x_{cycled}$ by $G$, so that the difference $L_cycle$ between the original and cycled photo can be used to train both generators, in order to improve their ability to actually transform between the 2 domains and not just generate new instances in each.

![CycleGAN forward](./doc/forward.png)

#### Backward

The real Monet painting $y_{real}$ is fed to $G$, which transforms it into a fake photo $x_{fake}$. The photo discriminator $D_X$ receives $x_{fake}$ and $x_{real}$, classifies them resulting in the loss $L_X$ that is used to train both $G$ and $D_X$, just like a regular GAN. Unlike a regular GAN, the fake photo $x_{fake}$ is transformed back to a Monet painting $y_{cycled}$ by $F$, so that the difference $L_cycle$ between the original and cycled Monet painting can be used to train both generators, in order to improve their ability to actually transform between the 2 domains and not just generate new instances in each.

![CycleGAN backward](./doc/backward.png)

#### Identity

Additionally, to help the generators identify features of the domains, they are also fed images from the target domain and are trained based on how well they return the same image. For example, $F$ is given the Monet painting $y_{real}$ and is trained based on $L_{identity}$, that is, how similar its output $y_{identity}$ is to $y_{real}$.

![CycleGAN identity](./doc/identity.png)

After all of the transformations above are applied, the losses are calculated and applied in the following way, weighted by the hyperparameter $\lambda$:
- $F$: $L_Y + \lambda * L_{cycle} + \frac{\lambda}{2} * L_{identity}$
- $G$: $L_X + \lambda * L_{cycle} + \frac{\lambda}{2} * L_{identity}$
- $D_X$: $L_X$
- $D_Y$: $L_Y$

$\lambda$ can be used to amplify/reduce the significance of creating a mapping between instances in the two domains, as opposed to the actual style transfer.

## Evaluation

### MiFID

Submissions are evaluated on MiFID (Memorization-informed Fréchet Inception Distance), which is a modification from [Fréchet Inception Distance (FID)](https://arxiv.org/abs/1706.08500).

The smaller MiFID is, the better your generated images are.

### What is FID?

Originally published [here](https://arxiv.org/abs/1706.08500) ([github](https://github.com/bioinf-jku/TTUR)), FID, along with [Inception Score (IS)](https://arxiv.org/abs/1606.03498), are both commonly used in recent publications as the standard for evaluation methods of GANs.

In FID, we use the Inception network to extract features from an intermediate layer.
Then we model the data distribution for these features using a multivariate Gaussian distribution with mean $\mu$ and covariance $\Sigma$.
The FID between the real images $r$ and generated images $g$ is computed as:

$$
\text{FID} = ||\mu_r - \mu_g||^2 + \text{Tr} (\Sigma_r + \Sigma_g - 2 (\Sigma_r \Sigma_g)^{1/2})
$$

where $Tr$ sums up all the diagonal elements. FID is calculated by computing the Fréchet distance between two Gaussians fitted to feature representations of the Inception network.

### What is MiFID (Memorization-informed FID)?

In addition to FID, Kaggle takes training sample memorization into account.

The memorization distance is defined as the minimum cosine distance of all training samples in the feature space, averaged across all user generated image samples. This distance is thresholded, and it's assigned to 1.0 if the distance exceeds a pre-defined epsilon.

In mathematical form:

$$
d_{ij} = 1 - cos(f_{gi}, f_{rj}) = 1 - \frac{f_{gi} \cdot f_{rj}}{|f_{gi}| |f_{rj}|}
$$

where $f_g$ and $f_r$ represent the generated/real images in feature space (defined in pre-trained networks);
and $f_{gi}$ and $f_{rj}$ represent the $i^{th}$ and $j^{th}$ vectors of $f_g$ and $f_r$, respectively.

$$
d = \frac{1}{N} \sum_{i} \min_j d_{ij}
$$

defines the minimum distance of a certain generated image ($i$) across all real images ($j$), then averaged across all the generated images.

![](https://storage.googleapis.com/kaggle-media/competitions/GAN/latex-img-150.png)

defines the threshold of the weight only applies when the ($d$) is below a certain empirically determined threshold.

Finally, this memorization term is applied to the FID:

$$
MiFID = FID \cdot \frac{1}{d_{thr}}
$$

### Kaggle's workflow calculating MiFID for public and private scores

Kaggle calculates public MiFID scores with the pre-train neural network [Inception](http://download.tensorflow.org/models/image/imagenet/inception-2015-12-05.tgz), and the public images used for evaluation are the rest of the TFDS Monet paintings.
**Note that as a Getting Started competition there is no private leaderboard.**

A demo of our MiFID evaluation code can be seen [here](https://www.kaggle.com/wendykan/demo-mifid-metric-for-dog-image-generation-comp).

### Submission File

You are going to generate 7,000-10,000 Monet-style images that are in `jpg` format.
Their sizes should be 256x256x3 (RGB).
Then you need to zip those images and your output from your Kernel should only have **ONE** output file named `images.zip`.

Please note that Kaggle Kernels has a number of output files capped at 500.
We highly encourage you to either directly write to a `zip` file as you generate images, or create a folder at `../tmp` as your temporary directory.


## Results

### Visualization

The model was trained for 30 epochs on a dataset of Monet-style images.

The results are presented below.

| ![Results](./doc/results.jpg) | ![Results2](./doc/results2.jpg) | ![Results3](./doc/results3.jpg) |
|:-----------------------------:|:-------------------------------:|:-------------------------------:|


The model was able to generate Monet-style images that are visually similar to the original paintings.

The model could be further improved by training on a larger dataset and for more epochs.
The only problem with this is that we used all the Monet-style images, and there are no more available.
Additionally, the hyperparameters could be tuned to improve the quality of the generated images.

### Thoughts

The goal was to reach the Top 25% which is equal to 52 Points, the samller the better. We achived 65 Points which is not as good as we were hoping for. We tried optimizing our hyperparameters as much as we could but we couldn't produce higher score than that.

## Gradio

We also created a [Gradio](https://www.gradio.app/) interface for the model, which allows users to upload their own photos and generate Monet-style images.

The interface is presented below.

![Gradio interface](./doc/Gradio_ui.png)

![Gradio interface](./doc/Gradio2.png)

<div style="page-break-after: always;"></div>

## Ethics and Fair Use in AI Art

Artificial Intelligence (AI) has opened up new possibilities in the field of art, enabling the creation of unique and innovative works.
However, the use of AI in art also raises several ethical and legal questions.

### Ethical Considerations

AI-generated art blurs the line between human creativity and machine learning. While AI can generate art, it does so based on patterns and algorithms, not personal experiences or emotions.
This raises questions about the authenticity and originality of AI-generated art.

Moreover, AI learns from existing works of art to generate new ones.
This could potentially lead to the appropriation of styles or elements from artists without their consent, which raises ethical concerns about respect for original creators.

Although the [EU AI Act](https://www.europarl.europa.eu/RegData/etudes/BRIE/2021/698792/EPRS_BRI(2021)698792_EN.pdf) clearly states that a **detailed summary of the content used in training** should be provided, this is not yet a universally accepted principle.
In our case we used Monet-style images to train our model, which is not a problem as Monet's works are in the public domain, but our model could be easily trained on copyrighted works.

## Different View Points on AI usage

### Is AI copyright is overreacted?

If we consider AI as a tool, why separate it into a different category than the brush or paint we use to create our masterpiece?
It seems that we only look at AI so harshly because it is a new technology and we feel it is a fraud. People who don't want to use it feel betrayed.
A strong case can be made that AI creations should be classified as art.
Suppose someone paints a picture without a brush, using only their hands, and then complains that it didn't turn out exactly as they wanted because they used the right tool.
We would laugh, but isn't that the same category as complaining about the art of artificial intelligence but not using it?
But if the masses like it, what can you do? Accept it and use it as a tool yourself!

Another interesting topic is the copyright problem of AI art, since a well-designed model can learn and reproduce anyone's work at an amazingly high level.
We may feel that people are only complaining about him because he does it too well.
Yes, AI learns from the works of others and tries to build on them to create new works, but isn't that exactly what humans do all their lives?
The only problem is that we don't want to accept that AI learns faster and does a better job with a few hours of learning than average humans, who need thousands of hours to reach the same level of knowledge.
If a person could copy someone so incredibly well, we would praise them, not complain about them.

AI is just a tool, like a paintbrush or paint
 We need to get used to it and integrate it into our style, rather than attack it.

### AI is cheating!

In fact, artificial intelligence can copy artists' work extremely well and create in their style.
It is difficult to argue whether this is theft or not, as it can undoubtedly cause harm to the artist.
Until there is a law to decide what to do in such cases, it should be considered theft.
I think what makes it all so terrible is that we currently use whatever data we can find on the internet to train our models, and no one gets credit if the model the masses are using is trained on their work.
So the problem goes much deeper than that, because it would hurt the artist a lot less if they got some recognition for the models they taught their work.
Paying artists would solve the copyright issue in one fell swoop, as it would no longer be theft.

It is important to note that with artificial intelligence, anyone can create fake works of art that can be sold as being created by someone else, causing damage to them.
At some point, it will be impossible to distinguish human art from AI art, and this will cause serious legal problems.
Which is not the case for non-AI copying or theft, because as mentioned in the previous section, AI is much better at copying artwork than humans, and you no longer need any skill to copy artwork anymore.

We also have to mention that it is very easy to create inappropriate content, and because AI can be so addictive, it can cause a lot of problems for the newer generation to have such easy access to, for example, highly addictive sexual or other harmful content, not to mention that you can't even trace the creator.
Who can be held to account in such a case.
The creator of the AI model or the user?

### Conclusion

The use of artificial intelligence cannot be banned in the long term, but very serious restrictions will be needed in the future on how models can be published.

AI is attracting a lot of people to art and making it more popular, which is very good.
However, this is currently hurting artists rather than helping them.
And this is not a negligible fact, because if we kill the artists, sooner or later there will be nothing truly man-made.

### Fair Use and Copyright

In terms of copyright, AI-generated art presents a complex issue.
If an AI uses existing works of art to generate new ones, who owns the copyright?
Is it the original artist, the AI programmer, or the AI itself?

However, the concept of "fair use" could potentially apply to AI-generated art.
Fair use allows limited use of copyrighted material without permission from the copyright holder for purposes such as criticism, parody, news reporting, research, and teaching.
If an AI-generated artwork is transformative, meaning it adds something new or different with a further purpose or different character, it could potentially fall under fair use.

### Conclusion

The use of AI in art is a rapidly evolving field with many ethical and legal implications.
As we continue to explore this new frontier, it's important to have ongoing discussions about how to ensure respect for original creators while also encouraging innovation and creativity.

<div style="page-break-after: always;"></div>

## Bibliography

- [Keras CycleGan](https://keras.io/examples/generative/cyclegan/)
- [CycleGan Tutorial](https://www.kaggle.com/code/amyjang/monet-cyclegan-tutorial)
- [Generative CycleGan](https://www.tensorflow.org/tutorials/generative/cyclegan)
- [CycleGan Tensorflow](https://hardikbansal.github.io/CycleGANBlog/)
- [Hugging Face](https://huggingface.co/docs/hub/spaces-sdks-gradio)
- [Cycle-Consistent Adversarial Networks](https://junyanz.github.io/CycleGAN/)
- [Generative Adversarial Networks (GANs) - Computerphile](https://www.youtube.com/watch?v=Sw9r8CL98N0)
- [Zebras, Horses & CycleGAN - Computerphile](https://www.youtube.com/watch?v=T-lBMrjZ3_0)
- [Gradio](https://www.gradio.app/)